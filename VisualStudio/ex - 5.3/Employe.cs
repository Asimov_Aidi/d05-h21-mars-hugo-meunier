﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___5._3
{
    partial class Employe
    {
        public Employe(int id, string name, string lastName, DateTime date, long NAS)
        {
            this.Id = id;
            this.FirstName = name;
            this.LastName = lastName;
            this.Birthday = date;
            this.SocialInsuranceNum = NAS;
        }
        public void afficher()
        {
            Console.WriteLine("id : " + this.Id );
            Console.WriteLine("prenom : " + this.FirstName);
            Console.WriteLine("nom de famille : " + this.LastName);
            Console.WriteLine("date naissance : " + this.Birthday);
            Console.WriteLine("NAS : " + this.SocialInsuranceNum);
        }
    }
}
