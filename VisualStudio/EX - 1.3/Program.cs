﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int resultat = 0;
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                int e = int.Parse(Console.ReadLine());
                resultat += e;
            }
            Console.Write(resultat);
            Console.ReadLine();
        }
    }
}
