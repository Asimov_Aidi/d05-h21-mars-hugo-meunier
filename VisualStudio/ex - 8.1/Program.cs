﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___8._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s = new Student(1) {Name = "toto", Age = 7};
            Employee e = new Employee() { Name = "titi", Age = 18, Salary = 17 };
            ArrayList al = new ArrayList();
            al.Add(s);
            al.Add(e);
            foreach (object item in al)
            {
                if (item is Student)
                {
                    Console.WriteLine("Étudiant : " + ((Student)item).StuNumber + " - " + ((Student)item).Name);
                }
                else if (item is Employee)
                {
                    Console.WriteLine("Employer : " + ((Employee)item).Name + " - " + ((Employee)item).Salary);
                }
            }
            Console.ReadLine();
            
        }
    }
    abstract class Person
    {
        public int Age { get; set; }
        public string Name { get; set; }
    }
    class Student : Person
    {
        public int StuNumber { get;}
        public Student(int st)
        {
            this.StuNumber = st;
        }
    }
    class Employee : Person
    {
        public int Salary { get; set; }

    }
}
