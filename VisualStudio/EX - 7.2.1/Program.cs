﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EX___7._2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Door door = new Door { ID = 1, IsOpen = false };
            SafetyGuard safetyGuard = new SafetyGuard(door);
            Owner owner = new Owner(door);
            //door.doorEvent();
            door.IsOpen = true;
            Thread.Sleep(1000);
            door.IsOpen = false;
            door.IsOpen = true;
            door.IsOpen = true;
            //door.doorEvent();
            Console.ReadLine();
        }
    }
    class Door
    {
        public event EventHandler<DoorOpenEventArgs> doorOpenEvent;
        public int ID { get; set; }
        private bool isOpen;
        public bool IsOpen {
            get { return this.isOpen; }
            set {
                if (this.isOpen == false && value == true)
                {
                    this.doorEvent();
                    this.isOpen = value;
                }
                this.isOpen = value;
                
            }
        }
        public void doorEvent()
        {
            if (doorOpenEvent != null)
            {
                doorOpenEvent(this, new DoorOpenEventArgs());
            }
        }    
    }
    class DoorOpenEventArgs
    {
        public DateTime Time { get; set; }

        public DoorOpenEventArgs()
        {
            this.Time = DateTime.Now;
        }
    }
    class SafetyGuard
    {
        public Door Door { get; set; }
        public SafetyGuard(Door door)
        {
            this.Door = door;
            this.Door.doorOpenEvent += HostDoorHandler;
        }
        public void HostDoorHandler(object sender, DoorOpenEventArgs e)
        {
            Console.WriteLine("SafetyGuard, door " + ((Door)sender).ID + " is open : " + ((Door)sender).IsOpen + " " + e.Time);
        }
    }
    class Owner
    {
        public Door Door { get; set; }
        public Owner(Door door)
        {
            this.Door = door;
            this.Door.doorOpenEvent += HostDoorHandler;
        }
        public void HostDoorHandler(object sender, DoorOpenEventArgs e)
        {
            Console.WriteLine("Owner, door " + ((Door)sender).ID + " is : " + ((Door)sender).IsOpen + " " + e.Time);
        }
    }
}
