﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___4._6
{
    class Program
    {
        static void MinMaxArray(float [] data, out float minimum, out float maximum)
        {
            maximum = data[0];
            minimum = data[0];
            for (int i = 1; i < data.Length; i++)
            {
                if (data[i] > maximum)
                {
                    maximum = data[i];
                }
                if (data[i] < minimum)
                {
                    minimum = data[i];
                }
            }
            
        }
        static void Main(string[] args)
        {
            float[] data = { 1.5f, 0.7f, 8.0f };
            float min;
            float max;
            MinMaxArray(data, out min, out max);
            Console.WriteLine(min);
            Console.WriteLine(max);
            Console.ReadLine();

        }
    }
}
