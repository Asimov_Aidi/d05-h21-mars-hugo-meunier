﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Student
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool? IsInternationalStu { get; set; }
        public DateTime? Birthday { get; set; }

        public Student(string name, int age, bool? isInter, DateTime? birth)
        {
            this.Name = name;
            this.Age = age;
            this.IsInternationalStu = isInter;
            this.Birthday = birth;
        }

    }
    public class Admin
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public Admin(string name, string password)
        {
            
            this.Name = name;
            this.Password = password;

        }
    }
}
