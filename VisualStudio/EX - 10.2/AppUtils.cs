﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___10._2
{
    static class AppUtils
    {
        public static List<int> StringToListInt(string a)
        {
            if (a == null)
            {
                throw new ArgumentNullException();
            }
            
            List<int> lst1 = new List<int>();
            string numberToConvert = "";

            foreach (var item in a)
            {
                if (item.ToString() != ",")
                {
                    numberToConvert += item.ToString();
                }
                else if (item.ToString() == ",")
                {
                    try
                    {
                        lst1.Add(int.Parse(numberToConvert));
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine("argument invalide : " + e.Message);
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("format invalide : " + e.Message);
                    }
                    finally
                    {
                        numberToConvert = "";
                    }
                }
            }

            return lst1;
        }
    }
}
