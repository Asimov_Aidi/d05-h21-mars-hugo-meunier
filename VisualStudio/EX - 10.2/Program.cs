﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___10._2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            List<int> lst3 = AppUtils.StringToListInt("12, 3, 45, 56 ,");
            foreach (int item in lst3)
            {
                Console.WriteLine(item);
            }
            //2
            try
            {
                List<int> lst1 = AppUtils.StringToListInt(null);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("null : " + e.Message);
            }

            //3
            try
            {
                List<int> lst2 = AppUtils.StringToListInt("abc, 23, 34");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("argument invalide : " + e.Message);
            }
            

            Console.Read();
        }
    }
}
