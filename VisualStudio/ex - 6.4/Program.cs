﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._4
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = "int a =10;";
            ProgramHelper helper = new ProgramHelper();
            Console.WriteLine(helper.ConvertToCSharp(data));
            Console.WriteLine(helper.ConvertToVB(data));
            Console.WriteLine("code check if C# : " + helper.CodeCheckSyntax(data,"C#"));
            Console.ReadLine();

        }
    }
}
