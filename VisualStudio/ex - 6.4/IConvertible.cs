﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._4
{
    interface IConvertible
    {
        string ConvertToCSharp(string a);
        string ConvertToVB(string a);

    }
    interface ICodeChecker : IConvertible
    {
        bool CodeCheckSyntax(string chaine, string language);
    }
}
