﻿using BLL;

using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public StudentManager MyManager { get; set; }
        
        public MainWindow()
        {
            InitializeComponent();
            
            this.MyManager = new StudentManager();
            
        }
        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.Name.Text))
            {
                this.message.Content = "Les champs Name et Age sont obligatoires pour insérer un étudiant dans la table T_Student.";
            }
            else if (string.IsNullOrWhiteSpace(this.Age.Text))
            {
                this.message.Content = "Les champs Name et Age sont obligatoires pour insérer un étudiant dans la table T_Student.";
            }
            else if (int.Parse(this.Age.Text) < 16 || int.Parse(this.Age.Text) > 99)
            {
                this.message.Content = "L’âge doit être compris entre 16 et 99 ans. ";
            }
            else 
            {
                Student student = new Student(
                this.Name.Text,
                int.Parse(this.Age.Text),
                (bool?)this.Status.IsChecked,
                (DateTime?)this.Birthday.SelectedDate
                );
                MyManager.AddAction(student);
                this.message.Content = "Vous avez inséré un étudiant";
            }
            

        }
    }
}
