﻿using BLL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public AdminManager admin { get; set; }
        public Login()
        {
            this.admin = new AdminManager();
            InitializeComponent();
        }
        private void login_click(object sender, RoutedEventArgs e)
        {
            string nom = this.Nom.Text;
            Admin monAdmin = this.admin.loginAction(nom);
            if ( monAdmin == null|| monAdmin.Password != this.password.Text)
            {
                MessageBox.Show("erreur de login");
            }
            else
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.InitializeComponent();
                this.Hide();
                mainWindow.Show();
            }
           
        }
    }
}
