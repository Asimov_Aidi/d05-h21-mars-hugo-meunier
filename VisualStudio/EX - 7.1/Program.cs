﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___7._1
{
    class Program
    {
        public delegate void DGSaySomething(string s);

        static void Main(string[] args)
        {
            Person p = new Chinese();
            DGSaySomething dG = p.SaySomething;
            
            if (dG != null)
            {
                dG("something");
            }
            Console.ReadLine();
        }
    }
    class Person
    {
        public virtual void SaySomething(string s)
        {
            Console.WriteLine("person : " + s);
        }
    }
    class Chinese : Person
    {
        public override void SaySomething(string s)
        {
            Console.WriteLine("Chinese : " + s);
        }
    }
}
