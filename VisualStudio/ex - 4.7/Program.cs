﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___4._7
{
    class Program
    {
        static void Double(int entier, out int x2)
        {
            x2 = entier * 2;
        }
        static void Main(string[] args)
        {
            int a = 5;
            int r;
            Double(a, out r);
            Console.Write(r);
            Console.ReadLine();

        }
    }
}
