﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___5._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student[] etudiant = { new Student("hugo","D05"), new Student("john","d06"), new Student("jhonny","D01","logique","ISI","isi@isi-mte.com","5142242424")};
            foreach (Student item in etudiant)
            {
                item.afficher();
            }
            Console.Read();
        }
    }
}
