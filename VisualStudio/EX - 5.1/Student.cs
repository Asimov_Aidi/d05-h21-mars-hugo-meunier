﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___5._1
{
    class Student
    {
        public static int NbEtudiant { get; set; }
        public string Nom { get; set; }
        public string Cours { get; set; }
        public string Sujet { get; set; }
        public string Universite { get; set; }
        public string Courriel { get; set; }
        public string Tel { get; set; }

        public Student(string nom2, string cours2)
        {
            this.Nom = nom2;
            this.Cours = cours2;
        }
        public Student(string nom2, string cours2,string sujet2, string universite2, string courriel2, string tel2)
        {
            this.Nom = nom2;
            this.Cours = cours2;
            this.Sujet = sujet2;
            this.Universite = universite2;
            this.Courriel = courriel2;
            this.Tel = tel2;
        }
        public void afficher()
        {
            Console.WriteLine("nom : " + Nom);
            Console.WriteLine("cours : " + Cours);
            Console.WriteLine("sujet : " + Sujet);
            Console.WriteLine("université : " + Universite);
            Console.WriteLine("courriel : " + Courriel);
            Console.WriteLine("tel : " + Tel);
        }
    }
}
