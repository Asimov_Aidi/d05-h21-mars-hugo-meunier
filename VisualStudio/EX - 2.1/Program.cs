﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___2._1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            ushort a = 52130;
            sbyte b = -115;
            uint c  = 4825932;
            sbyte d = 97;
            short e = -10000;
            short f = 20000;
            byte g = 224;
            uint h = 970700000;
            sbyte i = 112;
            sbyte j = -44;
            int k = - -1000000;
            short l = 1990;
            ulong m = 123456789123456789;

            //2
            bool isMale = true;

            //3
            String hello = "hello";
            String world = "World";
            Object concat = hello + " " + world;
            Console.WriteLine(concat);
            Console.ReadLine();
        }
    }
}
