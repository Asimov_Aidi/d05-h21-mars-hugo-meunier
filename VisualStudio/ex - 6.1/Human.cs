﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._1
{
    class Human
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
    class Student : Human
    {
        public int Mark { get; set; }
    }
    class Worker : Human
    {
        public int Rate { get; set; }
        public int HoursWorked { get; set; }

        public void CalculateSalary()
        {
            int resultat = this.Rate * this.HoursWorked;
            Console.WriteLine(resultat);
        }
        public Worker(int rate, int hours)
        {
            this.Rate = rate;
            this.HoursWorked = hours;
        }
    }
}
