﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___7._2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            SchoolReceptionist receptionist = new SchoolReceptionist();
            Director director = new Director(receptionist);
            Accountant accountant = new Accountant(receptionist);
            Student student = new Student(receptionist);
            receptionist.teacherSickEvent("Monsieurs Meunier", 3);
            receptionist.teacherSickEvent("Monsieurs Duval", 5);
            Console.ReadLine();

        }
    }
    class SchoolReceptionist
    {
        public event EventHandler<TeacherSickArgs> teacherSick;

        public void teacherSickEvent(string n, int j)
        {
            if (teacherSick != null)
            {
                teacherSick(this, new TeacherSickArgs(n,j));
            }
        }
    }
    class TeacherSickArgs
    {
        public string Nom { get; set; }
        public int NbJour { get; set; }
        

        public TeacherSickArgs(string n, int j)
        {
            this.NbJour = j;
            this.Nom = n;
        }
    }
    class Director
    {
        public SchoolReceptionist Receptionist { get; set; }
        public Director(SchoolReceptionist sr)
        {
            this.Receptionist = sr;
            this.Receptionist.teacherSick += HostTeacherSick;
        }
        public void HostTeacherSick(object sender, TeacherSickArgs e)
        {
            Console.WriteLine("Director, teacher " + e.Nom + " is sick for : " + e.NbJour);
        }
    }
    class Accountant
    {
        public SchoolReceptionist Receptionist { get; set; }
        public Accountant(SchoolReceptionist sr)
        {
            this.Receptionist = sr;
            this.Receptionist.teacherSick += HostTeacherSick;
        }
        public void HostTeacherSick(object sender, TeacherSickArgs e)
        {
            Console.WriteLine("Accountant, teacher " + e.Nom + " is sick for : " + e.NbJour);
        }
    }
    class Student
    {
        public SchoolReceptionist Receptionist { get; set; }
        public Student(SchoolReceptionist sr)
        {
            this.Receptionist = sr;
            this.Receptionist.teacherSick += HostTeacherSick;
        }
        public void HostTeacherSick(object sender, TeacherSickArgs e)
        {
            Console.WriteLine("Student, teacher " + e.Nom + " is sick for : " + e.NbJour);
        }
    }
}
