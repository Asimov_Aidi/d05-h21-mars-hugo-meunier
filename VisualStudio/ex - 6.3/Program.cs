﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._3
{
    class Program
    {
        static void Main(string[] args)
        {
            //#1
            Concessionnaire c = new Concessionnaire();
            c.salaireAnnuel = 10000;
            c.nbVenteAnnuel = 100;
            c.GetMonthlySalary();
            Fleuriste f = new Fleuriste();
            f.salaireAnnuel = 10000;
            f.GetMonthlySalary();
            Console.ReadLine();
            //#2
            Computer computer = new Computer();
            computer.WriteData(new SmartPhone());
            computer.ReadData(new SmartPhone());
            computer.WriteData(new USBDisk());
            computer.ReadData(new USBDisk());
            Console.ReadLine();
        }
    }
}
