﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._3
{
    abstract class StorageDevice
    {
        internal abstract void Read();
        internal abstract void Write();
    }
    class USBDisk : StorageDevice
    {
        internal override void Read()
        {
            Console.WriteLine("USB Read");
        }

        internal override void Write()
        {
            Console.WriteLine("USB Write");
        }
    }
    class SmartPhone : StorageDevice
    {
        internal override void Read()
        {
            Console.WriteLine("SmartPhone Read");
        }

        internal override void Write()
        {
            Console.WriteLine("SmartPhone Write");
        }
    }
    class Computer
    {
        public void ReadData(StorageDevice device)
        {
            device.Read();
        }
        public void WriteData(StorageDevice device)
        {
            device.Write();
        }
    }
}
