﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._3
{
    abstract class Employee
    {
        internal string nom;
        internal string adresse;
        internal int salaireAnnuel;
        public abstract void GetMonthlySalary();

    }
    class Concessionnaire : Employee
    {
        internal int nbVenteAnnuel;
        public override void GetMonthlySalary()
        {
            int a =this.nbVenteAnnuel / 12;
            Console.WriteLine((this.salaireAnnuel / this.nbVenteAnnuel)*a);
        }
    }
    class Fleuriste : Employee
    {
        public override void GetMonthlySalary()
        {
            Console.WriteLine(this.salaireAnnuel / 12);
        }
    }
}
