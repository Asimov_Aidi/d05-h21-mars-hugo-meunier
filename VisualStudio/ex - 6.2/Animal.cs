﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._2
{
    class Animal
    {
        int age;
        string nom;
        string sexe;
        public virtual void son()
        {
            Console.WriteLine("Son");
        }
    }
    class Dog : Animal
    {
        public override void son()
        {
            Console.WriteLine("wouf");
        }
    }
    class Frog : Animal
    {
        public override void son()
        {
            Console.WriteLine("Grabite");
        }
    }
    class Cat : Animal
    {
        public override void son()
        {
            Console.WriteLine("miaow");
        }
    }
    class Kitten : Animal
    {
        public override void son()
        {
            Console.WriteLine("little miaow");
        }
    }
    class Tomcat : Animal
    {
        public override void son()
        {
            Console.WriteLine("weird miaow");
        }
    }
}
