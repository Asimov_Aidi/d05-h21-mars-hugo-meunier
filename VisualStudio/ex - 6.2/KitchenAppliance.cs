﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._2
{
    class KitchenAppliance
    {
        public string Brand { get; set; }
        public string ManufacturingDate { get; set; }

        public virtual void PowerOn()
        {

        }
    }
    class Stove : KitchenAppliance
    {
        public override void PowerOn()
        {
            Console.WriteLine("Stove on");
        }
    }
    class Microwave : KitchenAppliance
    {
        public override void PowerOn()
        {
            Console.WriteLine("Microwave on");
        }
    }
    class Person
    {
        public void UseKitchenAppliance(KitchenAppliance ka)
        {
            ka.PowerOn();
        }
    }
}
