﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___6._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal[] grup = {new Dog(), new Kitten(),new Frog(),new Cat(),new Tomcat() };
            foreach (Animal item in grup)
            {
                item.son();
            }
            Console.ReadLine();
            Console.WriteLine("ex - 2");
            Person p = new Person();
            p.UseKitchenAppliance(new Stove());
            p.UseKitchenAppliance(new Microwave());
            Console.ReadLine();
        }
    }
}
