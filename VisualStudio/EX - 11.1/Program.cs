﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___11._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Remove();
            Console.ReadLine();

        }
        static void ajout()
        {
            Student stu = new Student();
            Console.WriteLine("Add a new student:");
            Console.Write("Name : ");
            stu.Name = Console.ReadLine();

            Console.Write("Age : ");
            stu.Age = Convert.ToInt32(Console.ReadLine());

            Console.Write("International Student(y/n) : ");
            stu.IsInternationalStu = Console.ReadLine() == "y" ? true : false;

            Console.Write("Hobbies : ");
            string usrInputHoobies = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputHoobies))
            {
                stu.Hobbies = null;
            }
            else
            {
                stu.Hobbies = usrInputHoobies;
            }

            Console.Write("Years of Experience : ");
            string usrInputYoE = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputYoE))
            {
                stu.YearsOfExperience = null;
            }
            else
            {
                stu.YearsOfExperience = Convert.ToInt32(usrInputYoE);
            }

            Console.Write("DOB (yyyy-mm-dd) : ");
            string usrInputDOB = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputDOB))
            {
                stu.DOB = null;
            }
            else
            {
                stu.DOB = Convert.ToDateTime(usrInputDOB);
            }

            using (SqlConnection conn = new SqlConnection(connectionString: @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=MyTest;Integrated Security=True;Connect Timeout=30"))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"insert into Student(name, age, isInternationalStu, YearsOfExperience, hobbies, Dob)
                                        values(@name, @age, @intl,@yearsofExperience,@hobbies, @dob)";

                    cmd.Parameters.AddWithValue("@name", stu.Name);
                    cmd.Parameters.AddWithValue("@age", stu.Age);
                    cmd.Parameters.AddWithValue("@intl", stu.IsInternationalStu);
                    if (stu.YearsOfExperience != null)
                    {
                        cmd.Parameters.AddWithValue("@yearsofExperience", stu.YearsOfExperience);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@yearsofExperience", DBNull.Value);
                    }

                    if (stu.Hobbies != null)
                    {
                        cmd.Parameters.AddWithValue("@hobbies", stu.Hobbies);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@hobbies", DBNull.Value);
                    }

                    if (stu.DOB != null)
                    {
                        cmd.Parameters.AddWithValue("@dob", stu.DOB);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@dob", DBNull.Value);
                    }

                    cmd.ExecuteNonQuery();
                }
            }

            Console.WriteLine("Added!!!");
        }
        static Student SearchById(string idStr)
        {
            Student stu = new Student();
            string connStr = @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=MyTest;Integrated Security=True;Connect Timeout=30";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    

                    cmd.CommandText = @"select id,age,name,DoB, isInternationalStu,YearsOfExperience, Hobbies
                        from Student where id = @id";
                    cmd.Parameters.Add(new SqlParameter("@id", idStr));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            
                            stu.Id = reader.GetInt64(0);
                            stu.Age = reader.GetInt32(1);
                            stu.Name = reader.GetString(2);
                            if (reader.GetValue(3) != DBNull.Value)
                            {
                                stu.DOB = reader.GetDateTime(3);
                            }
                            else
                            {
                                stu.DOB = null;
                            }

                            stu.IsInternationalStu = reader.GetBoolean(4);

                            if (reader.GetValue(5) != DBNull.Value)
                            {
                                stu.YearsOfExperience = reader.GetInt32(5);
                            }
                            else
                            {
                                stu.YearsOfExperience = null;
                            }


                            if (reader.GetValue(6) != DBNull.Value)
                            {
                                stu.Hobbies = reader.GetString(6);
                            }
                            else
                            {
                                stu.Hobbies = null;
                            }
                            
                        }
                    }
                    stu.Print();
                    return stu;
                }
            }
        }
        static void SearchByName(string nameStr)
        {
            Student stu = new Student();
            string connStr = @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=MyTest;Integrated Security=True;Connect Timeout=30";
            using (SqlConnection conn = new SqlConnection(connStr))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {


                    cmd.CommandText = @"select id,age,name,DoB, isInternationalStu,YearsOfExperience, Hobbies
                        from Student where name = @name";
                    cmd.Parameters.Add(new SqlParameter("@name", nameStr));

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {

                            stu.Id = reader.GetInt64(0);
                            stu.Age = reader.GetInt32(1);
                            stu.Name = reader.GetString(2);
                            if (reader.GetValue(3) != DBNull.Value)
                            {
                                stu.DOB = reader.GetDateTime(3);
                            }
                            else
                            {
                                stu.DOB = null;
                            }

                            stu.IsInternationalStu = reader.GetBoolean(4);

                            if (reader.GetValue(5) != DBNull.Value)
                            {
                                stu.YearsOfExperience = reader.GetInt32(5);
                            }
                            else
                            {
                                stu.YearsOfExperience = null;
                            }


                            if (reader.GetValue(6) != DBNull.Value)
                            {
                                stu.Hobbies = reader.GetString(6);
                            }
                            else
                            {
                                stu.Hobbies = null;
                            }

                        }
                    }
                    stu.Print();
                }
            }
        }
        static void Modify()
        {
            string id = "5";
            Student toModify = SearchById(id);

            Console.Write("Name : ");
            toModify.Name = Console.ReadLine();

            Console.Write("Age : ");
            toModify.Age = Convert.ToInt32(Console.ReadLine());

            Console.Write("International Student(y/n) : ");
            toModify.IsInternationalStu = Console.ReadLine() == "y" ? true : false;

            Console.Write("Hobbies : ");
            string usrInputHoobies = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputHoobies))
            {
                toModify.Hobbies = null;
            }
            else
            {
                toModify.Hobbies = usrInputHoobies;
            }

            Console.Write("Years of Experience : ");
            string usrInputYoE = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputYoE))
            {
                toModify.YearsOfExperience = null;
            }
            else
            {
                toModify.YearsOfExperience = Convert.ToInt32(usrInputYoE);
            }

            Console.Write("DOB (yyyy-mm-dd) : ");
            string usrInputDOB = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(usrInputDOB))
            {
                toModify.DOB = null;
            }
            else
            {
                toModify.DOB = Convert.ToDateTime(usrInputDOB);
            }

            using (SqlConnection conn = new SqlConnection(connectionString: @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=MyTest;Integrated Security=True;Connect Timeout=30"))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"update Student 
                    Set name = @name, age = @age, isInternationalStu = @intl, YearsOfExperience = @yearsofExperience, hobbies = @hobbies, DoB = @dob 
                    Where id = @id";

                    cmd.Parameters.AddWithValue("@id", toModify.Id);
                    cmd.Parameters.AddWithValue("@name", toModify.Name);
                    cmd.Parameters.AddWithValue("@age", toModify.Age);
                    cmd.Parameters.AddWithValue("@intl", toModify.IsInternationalStu);
                    if (toModify.YearsOfExperience != null)
                    {
                        cmd.Parameters.AddWithValue("@yearsofExperience", toModify.YearsOfExperience);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@yearsofExperience", DBNull.Value);
                    }

                    if (toModify.Hobbies != null)
                    {
                        cmd.Parameters.AddWithValue("@hobbies", toModify.Hobbies);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@hobbies", DBNull.Value);
                    }

                    if (toModify.DOB != null)
                    {
                        cmd.Parameters.AddWithValue("@dob", toModify.DOB);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@dob", DBNull.Value);
                    }

                    cmd.ExecuteNonQuery();
                }
            }
        }
        static void Remove()
        {
            string id = "8";
            Student toModify = SearchById(id);

            using (SqlConnection conn = new SqlConnection(connectionString: @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=MyTest;Integrated Security=True;Connect Timeout=30"))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"DELETE FROM Student Where id = @id";

                    cmd.Parameters.AddWithValue("@id", toModify.Id);
                    
                    
                    cmd.ExecuteNonQuery();
                }
            }
            Console.WriteLine("Student remove ");
        }
    }
}
