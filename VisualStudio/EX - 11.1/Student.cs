﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___11._1
{
    class Student
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsInternationalStu { get; set; }
        public string Hobbies { get; set; }
        public int? YearsOfExperience { get; set; }
        public DateTime? DOB { get; set; }

        public void Print()
        {
            Console.WriteLine("Id:" + this.Id);
            Console.WriteLine("Name:" + this.Name);
            Console.WriteLine("Age:" + this.Age);
            Console.WriteLine("IsInternationalStu:" + this.IsInternationalStu);
            Console.WriteLine("Hobbies:" + this.Hobbies);
            Console.WriteLine("YearsOfExperience:" + this.YearsOfExperience);
            Console.WriteLine("DOB:" + this.DOB);
            Console.WriteLine("---------------------------------------");
        }
    }
}
