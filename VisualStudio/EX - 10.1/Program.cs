﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___10._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Ch10Ex1Method1();
            Ch10Ex1Method2();
            Ch10Ex1Method3();
            Ch10Ex1Method4();

            Console.ReadLine();
        }
        //1
        static void Ch10Ex1Method1()
        {
            try
            {
                int i = 0;
                int res1 = 45 / i;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        //2
        static void Ch10Ex1Method2()
        {
            try
            {
                int[] arrInt = new int[20];
                arrInt[20] = 15;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        //3
        static void Ch10Ex1Method3()
        {
            try
            {
                int[] arrInt = null;
                arrInt[20] = 15;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
        //4
        static void Ch10Ex1Method4()
        {
            try
            {
                DateTime dt = DateTime.Parse("abc");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.GetType());
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
