﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___8._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Nombre premier entre 0 et 100 : ");
            List<int> intList = new List<int>();
            intList = GetPrimes(0, 100);
            foreach (int item in intList)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();

        }
        static List<int> GetPrimes(int start, int end)
        {
            List<int> intList = new List<int>();
           
            for (int i = start; i < end; i++)
            {
                int j = 2;
                while (j < i && i % j != 0)
                {
                    j += 1;
                    if (j == i)
                    {
                        intList.Add(i);
                    }
                }
            }
            return intList;
        }
    }
}
