﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX___10._3
{
    class StudentAgeException : Exception
    {

    }
    class Student
    {
        private int age;
        public int Age
        {
            get { return Age; }
            set {
                if (value > 100 || value < 16)
                {
                    throw new StudentAgeException();
                }
                else
                {
                    this.age = value;
                }
            }
        }
        public string prenom { get; set; }
        public string nom { get; set; }
        public int id { get; set; }
        public bool sexe { get; set; }

    }
}
