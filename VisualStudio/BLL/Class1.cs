﻿using DAL;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AdminManager
    {
        public AdminService MyAdmin { get; set; }
        public AdminManager()
        {
            this.MyAdmin = new AdminService();
        }
        public Admin loginAction(string name)
        {
            
            return this.MyAdmin.LoginAdmin(name);
        }
    }
    public class StudentManager
    {
        public StudentService MyStudent { get; set; }
        public StudentManager()
        {
            this.MyStudent = new StudentService();

        }
        public void AddAction(Student s)
        {
            this.MyStudent.AddStudent(s);


        }
    }
}
