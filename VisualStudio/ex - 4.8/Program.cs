﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ex___4._8
{
    class Program
    {
        static int Add(params int[] values)
        {
            int answer = 0;
            foreach (int value in values)
            {
                answer += value;
            }
            return answer;
        }
        static void Main(string[] args)
        {
            int a = Add(1, 3, 4, 2, 5);
            Console.Write(a);
            Console.ReadLine();

        }
    }
}
