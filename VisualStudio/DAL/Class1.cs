﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class AdminService
    {
        Admin admin = null;
        private static readonly string connectionString = @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=DBExamenFinalCSharp;Integrated Security=True;Connect Timeout=5";
        public Admin LoginAdmin(string nom)
        {
            
            
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"select name, password from T_Admin where name= @nom";
                    cmd.Parameters.Add(new SqlParameter("@nom", nom));
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            admin = new Admin(reader.GetString(0), reader.GetString(1));
                           
                           
                            
                        }
                    }
                }
            }
            return admin;
        }
    }
    public class StudentService
    {
        private static readonly string connectionString = @"Data Source=751FJW2\SQLEXPRESS;Initial Catalog=DBExamenFinalCSharp;Integrated Security=True;Connect Timeout=5";

        public void AddStudent(Student newStudent)
        {

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {

                    cmd.CommandText = "insert into T_Student( name, age, birthday, isInternationalStudent)"
                                   + "values('" + newStudent.Name + "', '" + newStudent.Age + "', @bithday, @inter)";
                    if (newStudent.Birthday != null)
                    {
                        cmd.Parameters.AddWithValue("@bithday", newStudent.Birthday);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@bithday", DBNull.Value);
                    }
                    if (newStudent.IsInternationalStu != null)
                    {
                        cmd.Parameters.AddWithValue("@inter", newStudent.IsInternationalStu);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@inter", DBNull.Value);
                    }

                    cmd.ExecuteNonQuery();
                }


            }

        }
    }
}
